import React, {useEffect} from 'react';
import './Cashbox.scss';
import Footer from "../../components/Footer";
import CashboxContent from "../../components/Cashbox";
import Header from "../../components/RoomsHeader";
import {useDispatch, useSelector} from "react-redux";

const Cashbox = () => {
  const user = useSelector(state => state.app.user);
  const dispatch = useDispatch();

  return (
    <>
      <Header/>
        <div className="cashbox_grid">
          <CashboxContent />
        </div>
      <Footer xs={7}/>
    </>
  );
};

export default Cashbox;

import React from 'react'
import { Grid } from '@material-ui/core'
import './RoomSetting.scss'
import Footer from '../../components/Footer'
import Header from '../../components/RoomsHeader'
import Setting from '../../components/RoomSetting'
import { useParams } from 'react-router-dom'

const RoomSetting = () => {
  const params = useParams()

  return (
    <>
      <Header />
      <Grid container justify="center">
        <Grid className="grid_options" item xs={7} >
          <div className="rooms_title">Настройки комнаты {params.roomId}</div>
          <Setting optionWindow={'editroom'} />
        </Grid>
      </Grid>
      <Footer />
    </>
  )
}

export default RoomSetting

import React from 'react'
import { Grid } from '@material-ui/core'
import './WebinarSetting.scss'
import Footer from '../../components/Footer'
import Header from '../../components/RoomsHeader'
import Setting from '../../components/WebinarSetting'
import { useParams } from 'react-router-dom'

const WebinarSetting = () => {
  const params = useParams()

  return (
    <>
      <Header />
      <Grid container justify="center">
        <Grid className="grid_options" item xs={7} >
          <div className="rooms_title">Настройки вебинара {params.webinarId}</div>
          <Setting />
        </Grid>
      </Grid>
      <Footer />
    </>
  )
}

export default WebinarSetting

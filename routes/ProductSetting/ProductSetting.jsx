import React from 'react'
import './ProductSetting.scss'
import Footer from '../../components/Footer'
import Header from '../../components/RoomsHeader'
import Setting from '../../components/ProductSetting'
import TitleForRouteScreen from '../../components/UI/TitleForRouteScreen'
import { useParams } from 'react-router-dom'
import { Grid } from '@material-ui/core'

const ProductSetting = () => {
  const params = useParams()

  return (
    <>
      <Header />
      <Grid container justify="center">
        <Grid className="grid_options" item xs={7} >
          <TitleForRouteScreen>
            Настройки товара {params.roomId}
          </TitleForRouteScreen>
          <Setting />
        </Grid>
      </Grid>
      <Footer />
    </>
  )
}

export default ProductSetting

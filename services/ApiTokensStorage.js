import reduxStore from './StoreService';

import {deleteTokens, updateTokens} from '../lib/auth/actions';

const store = reduxStore.get();

class ApiTokensStorage {
  getTokens() {
    const state = store.getState();

    return state.auth.tokens || {};
  }

  setTokens(tokens) {
    store.dispatch(updateTokens(tokens));
  }

  clearTokens() {
    store.dispatch(deleteTokens());
  }
}

export default ApiTokensStorage;

import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import { persistReducer, persistStore, createMigrate } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { createBlacklistFilter } from 'redux-persist-transform-filter'
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync'
// import { crosstabSync } from '../utils/crosstabSync'

import rootReducer from '../lib'

const chat = createBlacklistFilter(
  'app',
  ['chat']
)

const counter = createBlacklistFilter(
  'loader',
  ['counter']
)

const migrations = {
  '0': (state) => {
    return {
      ...state,
    }
  },
  '3': (state) => {
    return {
      ...state,
      admin: { ...state.admin, rooms: { ...state.admin.rooms, room: {} } }
    }
  },
  '4': (state) => {
    return {
      ...state,
      app: { ...state.app, webinar: { webinarPublic: {} } }
    }
  },
  '5': (state) => {
    return {
      ...state,
      admin: { ...state.admin, rooms: { ...state.admin.rooms, currentRoom: state.admin.rooms.room } }
    }
  }
}

const persistConfig = {
  key: 'webinar',
  storage,
  whitelist: [
    'auth',
    'app',
    'navigation',
    'admin'
  ],
  transforms: [
    chat,
    counter
  ],
  version: 5,
  migrate: createMigrate(migrations, { debug: false }),
}

const stateSyncConfig = {
  channel: 'crossTabSync',
  blacklist: ['persist/REHYDRATE', 'persist/PERSIST', 'APP_CHAT_MESSAGES_WS'],

}

class StoreService {
  constructor () {
    this.store = this.createStore()
    initMessageListener(this.store)
    this.persistor = new Promise(resolve => persistStore(this.store, null, () => resolve()))

    //crosstabSync(this.store, persistConfig, KEY_PREFIX, REHYDRATE)
  }

  createStore () {
    const persistedReducer = persistReducer(persistConfig, rootReducer)
    const __REDUX_DEVTOOLS_EXTENSION__ = window.__REDUX_DEVTOOLS_EXTENSION__
    const initialState = {}
    const middlewares = [thunk, createStateSyncMiddleware(stateSyncConfig)]

    return createStore(
      persistedReducer,
      initialState,
      compose(
        applyMiddleware(...middlewares),
        (__REDUX_DEVTOOLS_EXTENSION__ && process.env.REACT_APP_APP_ENV !== 'production')
          ? __REDUX_DEVTOOLS_EXTENSION__() : f => f
      )
    )
  }

  get () {
    return this.store
  }

  getPersistor () {
    return this.persistor
  }
}

const store = new StoreService()

export default store

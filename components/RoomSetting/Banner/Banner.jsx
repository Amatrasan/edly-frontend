import React, { useEffect, useRef, useState } from 'react'
import './Banner.scss'
import { deleteBanner, postBanner, updateBanner } from '../../../lib/admin/banner/actions'
import Toggle from '../../UI/Toggle'
import PropTypes from 'prop-types'
import { alertbarShow } from '../../../lib/app/alertbar/actions'
import { useDispatch, useSelector } from 'react-redux'
import SettingFooter from '../Footer'
import PopUp from '../../UI/PopUp'
import { Link, useParams } from 'react-router-dom'
import { Delete, Href, Reduce, Save, Setting, Plus } from '../../UI/Icons/Icons'
import Button from '../../UI/Button'
import ButtonBorder from '../../UI/ButtonBorder'
import Input from '../../UI/Input'
import CreateProductForm from '../../CreateProductForm'
import { getProducts } from '../../../lib/admin/products/actions'
import { roomUpdateBanner } from '../../../lib/admin/rooms/actions'

const imgPath = [
  '/assets/img_fon_small/fon_small_1.jpg',
  '/assets/img_fon_small/fon_small_2.jpg',
  '/assets/img_fon_small/fon_small_3.jpg',
  '/assets/img_fon_small/fon_small_4.jpg',
  '/assets/img_fon_small/fon_small_5.jpg',
  '/assets/img_fon_small/fon_small_6.jpg',
  '/assets/img_fon_small/fon_small_7.jpg',
  '/assets/img_fon_small/fon_small_8.jpg',
  '/assets/img_fon_small/fon_small_9.jpg',
  '/assets/img_fon_small/fon_small_10.jpg',
  '/assets/img_fon_small/fon_small_11.jpg',
  '/assets/img_fon_small/fon_small_12.jpg',
  '/assets/img_fon_small/fon_small_13.jpg',
  '/assets/img_fon_small/fon_small_14.jpg',
  '/assets/img_fon_small/fon_small_15.jpg',
  '/assets/img_fon_small/fon_small_16.jpg',
  '/assets/img_fon_small/fon_small_17.jpg',
  '/assets/img_fon_small/fon_small_18.jpg'
]

const text = {
  unit: 'Руб.'
}

const Banner = (props) => {
  const account = useSelector(state => state.app.user.account)
  const products = useSelector(state => state.admin.products.products)
  const [bannerList, setBannerList] = useState([])
  const [showPopUp, setShowPopUp] = useState('')
  const [isShowProducts, setIsShowProducts] = useState(true)
  const params = useParams()
  const room = useSelector(state => state.admin.rooms.currentRoom[params.roomId])
  const dispatch = useDispatch()
  const selectedBanner = useRef(0)

  useEffect(() => {
    setBannerList(room ? room.banners : [])
  }, [room])

  useEffect(() => {
    dispatch(getProducts(1, 9999))
  }, [dispatch])

  const selectBannerBackground = (data) => {
    setBannerList((state) => state.map((banner, i) => {
      if (i === selectedBanner.current) {
        return { ...banner, image: data }
      } else {
        return banner
      }
    }))
  }

  const addBanner = () => {
    const temp = {
      room_id: room.id,
      product: null,
      image: '',
      title: '',
      url: ''
    }
    setBannerList([...bannerList, temp])
    setTimeout(() => showBanner(bannerList.length, true), 300)
  }

  const deleteSelectedBanner = () => {
    dispatch(deleteBanner(room.id, bannerList[selectedBanner.current])?.id)
  }

  const addProduct = (product) => {
    setBannerList((state) => state.map((banner, i) => {
      if (i === selectedBanner.current) {
        return { ...banner, product: product }
      } else {
        return banner
      }
    }))
    closePopUp()
  }

  const showBanner = (i, show) => {
    const bannerSmall = document.getElementById(`single_banner_small_${i}`)
    const bannerBig = document.getElementById(`single_banner_big_${i}`)
    bannerSmall.style.display = show ? 'none' : ''
    bannerBig.style.visibility = show ? 'visible' : 'hidden'

    if (show) {
      bannerBig.style.display = 'block'
    } else {
      setTimeout(() => {
        bannerBig.style.display = 'none'
      }, 300)
    }

    const banner = document.querySelectorAll('.setting_webinar_banner_single_banner')[i]
    banner.querySelector('.single_banner_big_header').style.display = show ? 'flex' : 'none'
    banner.classList.toggle('setting_webinar_banner_single_banner_big')
  }

  const changeTypeBanner = (index) => {
    setBannerList((state) => state.map((banner, i) => {
      if (i === index) {
        return { ...banner, product: banner.product ? null : {} }
      } else {
        return banner
      }
    }))
  }

  const bannerHandleInput = (index, property, value) => {
    setBannerList((state) => state.map((banner, i) => {
      if (i === index) {
        return { ...banner, [property]: value }
      } else {
        return banner
      }
    }))
  }

  const saveBanner = (banner) => {
    if (banner.id) {
      dispatch(updateBanner(banner.id, { ...banner, room_id: room.id, is_product: !!banner.product, product_id: banner?.product?.id, url: banner.url || '' })).then(res => {
        dispatch(roomUpdateBanner({ roomId: res.payload.room_id, banner: res.payload }))
      })
    } else {
      dispatch(postBanner(room.id, { ...banner, is_product: !!banner.product, product_id: banner?.product?.id, url: banner.url || '', title: banner.title }))
    }
  }

  const showProduct = (product, context) => {
    return (
      <div className={'setting_banner_product'} onClick={context === 'popup' ? () => addProduct(product) : null}>
        <div className={'setting_banner_product_main_info'}>
          {context === 'popup' ?
            <div className={'setting_banner_product_name'}>{product.name}</div> :
            <Link to={`/edit/product/${product.id}`}>{product.name}</Link>}
          <div>ID:{product.id}</div>
        </div>
        <div className={'setting_banner_product_cost'}>
          <span>{product.price.toLocaleString()}&nbsp;</span>
          <span>{text.unit}</span>
        </div>
      </div>
    )
  }

  const isDisabled = (banner) => {
    if (banner.image) {
      if (banner.product) {
        return !banner.product.id
      } else {
        return !(banner.url && banner.title)
      }
    } else {
      return true
    }
  }

  const delBanner = (banner, i) => {
    if (banner.id) {
      setShowPopUp('POPUP_DELETE_BANNER')
    } else {
      setBannerList((state) => state.filter((data, index) => i !== index))
    }
  }

  const eachAddBanners = (banner, i) => {
    return (
      <div className={'setting_webinar_banner_single_banner'} id={`banner_${i}`} key={i} onClick={() => selectedBanner.current = i}>
        <div className={'single_banner_small'} id={`single_banner_small_${i}`}>
          <div className={'single_banner_small_title'}>{banner.title}</div>
          <div className={'single_banner_small_price_or_url'}>
            {banner.product ? <div>{banner.product.price?.toLocaleString()}&nbsp;руб.</div> : <div>{banner.url}</div>}
          </div>
          <Setting className={'single_banner_small_icon'} onClick={() => showBanner(i, true)} />
        </div>
        <div className={`single_banner_big ${banner.product ? 'single_banner_big_product' : ''}`} id={`single_banner_big_${i}`}>
          <div className={'single_banner_big_header'}>
            <Toggle id={`single_banner_toggle_${i}`} onSelect={banner.id ? null : () => changeTypeBanner(i)} label={'Товар'} selected={!!banner.product} />
            <div className={'single_banner_big_header_controls'}>
              <Button size={'medium'} disabled={isDisabled(banner)} onClick={() => saveBanner(banner)}><Save/></Button>
              <Delete onClick={() => delBanner(banner, i)} />
              <Reduce className={'single_banner_big_header_controls_reduce'} onClick={() => showBanner(i, false)} />
            </div>
          </div>
          <div className={`input_ui_main ${''}`}>
            <div className="input_ui_title">{'Баннер'}</div>
            <div className={'single_banner_big_btn_choice_background'} onClick={() => setShowPopUp('POPUP_BACKGROUND')}>
              <span>{'Щелкните чтобы выбрать фон'}</span>
              {banner.image && <img src={banner.image} alt={'Фон'} />}
            </div>
          </div>
          <Input title={true} titleText={'Текст'} value={banner.title || ''} onChange={(e) => bannerHandleInput(i, 'title' , e.target.value)} />
          {banner.product ?
            <>
              <div className={`input_ui_main ${''}`}>
                <div className="input_ui_title">{'Привязанный товар'}</div>
                <div className={'single_banner_big_attached_product'}>
                  {banner.product.id ? showProduct(banner.product): null}
                  <div className={'single_banner_big_btn_choice_product'} onClick={() => setShowPopUp('POPUP_CHOICE_PRODUCT')}>
                    <Href/>
                    <span>Выбрать</span>
                  </div>
                </div>
              </div>
            </> :
            <>
              <Input title={true} titleText={'Ссылка'} placeholder={'URL-адрес, куда ведет баннер'} value={banner.url || ''} onChange={(e) => bannerHandleInput(i, 'url' , e.target.value)} />
            </>}
        </div>

      </div>
    )
  }

  const addBannerClickHandler = () => {
    if (!account.payment) {
      dispatch(alertbarShow({
        type: 'error',
        messages: ['Пожалуйста, сперва настройте интеграцию с платежной системой в разделе "Касса"']
      }))
    } else {
      addBanner()
    }
  }

  const closePopUp = () => {
    setShowPopUp('')
  }

  const showAddBanners = () => bannerList && bannerList.map(eachAddBanners)

  const eachProduct = (product) => {
    return showProduct(product, 'popup')
  }

  const showProducts = () => products.map(eachProduct)

  return (
    <>
      <PopUp
        title={'Выбор фона'}
        show={showPopUp === 'POPUP_BACKGROUND'}
        showButtonAccept={false}
        showButtonCancel={true}
        buttonCancelLabel={'Отмена'}
        closePopUp={closePopUp}
        onCancel={closePopUp}
        className="setting_room_banner_popup_choice_background"
      >
        <div className="fon_list">
          {imgPath.map((data, i) => (
            <div key={i} style={{ backgroundImage: `url(${data})` }}
              className="fon_list_single" onClick={() => {
                selectBannerBackground(data)
                closePopUp()
              }}>
            </div>
          ))}
        </div>
      </PopUp>
      <PopUp
        title={'Удаление баннера'}
        show={showPopUp === 'POPUP_DELETE_BANNER'}
        showButtonCancel={true}
        showButtonAccept={true}
        buttonAcceptColor={'red'}
        buttonAcceptLabel={'Удалить'}
        buttonCancelLabel={'Отмена'}
        onCancel={closePopUp}
        closePopUp={closePopUp}
        onAccept={deleteSelectedBanner}
        disableScroll={true}
        className={'setting_room_banner_popup_delete_banner'}
      >
        <div className={'setting_room_banner_popup_delete_banner__subtitle'}>
          Вы собираетесь удалить баннер - {bannerList[selectedBanner.current]?.title}
        </div>
      </PopUp>
      <PopUp
        title={'Выбор товара'}
        show={showPopUp === 'POPUP_CHOICE_PRODUCT'}
        showButtonAccept={false}
        showButtonCancel={isShowProducts && !!(products && products.length)}
        buttonCancelLabel={'Отмена'}
        onCancel={closePopUp}
        closePopUp={closePopUp}
        disableScroll={true}
        className={'setting_room_banner_popup_choice_product'}
      >
        <div>
          {(products && products.length) ? (
            <>
              <Toggle
                classList={'setting_room_banner_popup_choice_product_toggle'}
                selected={!isShowProducts}
                onSelect={() => setIsShowProducts(state => !state)}
                label={'Создать товар'}
              />
              {isShowProducts ? (
                <div className={'setting_room_banner_popup_choice_product_list'}>
                  {showProducts()}
                </div>
              ) : <CreateProductForm onSuccessCallback={() => setIsShowProducts(state => !state)} closePopUp={null} className={'setting_room_banner_popup_choice_product_form'}/>}
            </>
          ) : (
            <>
              <div className={'setting_room_banner_popup_choice_product_subtitle'}>У вас нет товаров</div>
              <div  className={'setting_room_banner_popup_choice_product_description'}>Создайте, чтобы привязать к баннеру</div>
              <CreateProductForm onCancel={closePopUp} className={'setting_room_banner_popup_choice_product_form'}/>
            </>

          )}
        </div>
      </PopUp>
      <div className={props.webinarLocation ? 'setting_webinar_banner' : 'setting_banner_padding'}>
        {bannerList && bannerList.length > 0 ? (
          <div className={'admin_header_setting_window_banner_list'}>
            {showAddBanners()}
          </div>
        ) : <></>}
        <div className="btn_add_banner" onClick={addBannerClickHandler}>
          <ButtonBorder size={'medium'}><Plus/></ButtonBorder>
          Добавить баннер
        </div>
        <SettingFooter hideSaveButton={true} rollUp={props.rollUp} webinar={props.webinarLocation} text={props.text} />
      </div>
    </>
  )
}

Banner.propTypes = {
  rollUp: PropTypes.func.isRequired,
  text: PropTypes.shape({
    bannerDelete: PropTypes.string,
    bannerSave: PropTypes.string
  }).isRequired,
  webinarLocation: PropTypes.bool
}

export default Banner

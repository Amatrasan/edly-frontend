import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import CircularProgress from '@material-ui/core/CircularProgress';

import './Loader.scss';

class Loader extends PureComponent {
  static propTypes = {
    isActive: PropTypes.bool.isRequired,
  };

  render() {
    return (
      <div className={cx('b-loader-container', {
        'b-loader_state_hidden': !this.props.isActive,
        'b-loader_state_visible': this.props.isActive,
      })}
      >
        <div className={cx('b-loader')}>
          <CircularProgress/>
        </div>
      </div>
    );
  }
}

export default Loader;

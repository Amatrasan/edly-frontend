import React, { useEffect, useState } from 'react'
import './Header.scss'
import Button from '../../components/UI/Button'
import PaymentWindow from '../PopupPutInDeposit'
import { Cabinet, CashBox, CloseIcon, OutIcon, WebinarIcon } from '../UI/Icons/Icons'
import { useDispatch, useSelector } from 'react-redux'
import { authLogout } from '../../lib/auth/actions'
import { Link, useHistory, useParams } from 'react-router-dom'
import { createAccountBalanceRefillOrder } from '../../lib/app/orders/actions'

const text = {
  popupBalance: 'Пополните счет, функциональность аккаунта ограничена.',
  out: 'Выйти',
  balance: 'Баланс',
  cashBox: 'Касса',
  webinars: 'Вебинары',
  rooms: 'Комнаты',
  deposit: 'Пополнить',
  privateCabinet: 'Личный кабинет'
}
const Header = () => {
  const [openMenu, setOpenMenu] = useState(false)
  const [openPayment, setOpenPayment] = useState(false)
  const dispatch = useDispatch()
  const user = useSelector(state => state.app.user)
  const balance = user.account ? user.account.balance : null

  const history = useHistory()
  const params = useParams()

  const logout = () => {
    dispatch(authLogout(history))
  }

  useEffect(() => {
    const rooms = document.getElementsByName('header_main_area_nav_menu_rooms')
    const cashbox = document.getElementsByName('header_main_area_nav_menu_cashbox')
    if (params.webinarId || params.roomId || history.location.pathname === '/rooms') {
      for (let i = 0; i < rooms.length; i++) {
        rooms[i].classList.add('header_main_area_nav_menu_option_selected')
      }
      for (let i = 0; i < cashbox.length; i++) {
        cashbox[i].classList.remove('header_main_area_nav_menu_option_selected')
      }
    }
    if (history.location.pathname === '/cashbox' || params.productId) {
      for (let i = 0; i < rooms.length; i++) {
        rooms[i].classList.remove('header_main_area_nav_menu_option_selected')
      }
      for (let i = 0; i < cashbox.length; i++) {
        cashbox[i].classList.add('header_main_area_nav_menu_option_selected')
      }
    }
  }, [history, params.productId, params.roomId, params.webinarId])

  const deposit = (amount) => {
    dispatch(createAccountBalanceRefillOrder(amount))
      .then((order) => {
        if (order) {
          const widget = new window.cp.CloudPayments()
          widget.charge(order,
            function (options) { // success
              // действие при успешной оплате
              setOpenPayment(false)
            },
            function (reason, options) { // fail
              // действие при неуспешной оплате
            })
        }
      })
  }

  if (!user.id) {
    return <></>
  }

  return (
    <header className="header_main">
      <div className={`header_main_right_menu ${openMenu ? 'selected' : ''}`} id="header_main_right_menu" tabIndex={7}>
        <div className="header_main_right_menu_content">
          <div className="header_main_right_menu_content_header">
            <div>
              {user.first_name}
            </div>
            <CloseIcon className="header_main_area_nav_menu_icon pointer" onClick={() => setOpenMenu(false)} />
          </div>
          <div className="header_main_right_menu_content_link_list">
            <Link className="header_main_area_nav_menu_option" name="header_main_area_nav_menu_rooms" to={user.owner ? '/rooms' : '/webinars'}>
              <WebinarIcon className="header_main_area_nav_menu_icon" />
              {user.owner ? text.rooms : text.webinars}
            </Link>
            {user.account && (
              <Link className="header_main_area_nav_menu_option" name="header_main_area_nav_menu_cashbox" to="/cashbox">
                <CashBox className="header_main_area_nav_menu_icon" />
                {text.cashBox}
              </Link>
            )}
            <div className="header_main_right_menu_line"/>
          </div>
          {user.account && (
            <div className="header_main_area_nav_menu_balance header_main_area_nav_menu_balance_right_menu">
              {text.balance}:&nbsp;<span style={{ color: balance > 0 ? '#00E096' : '#FF3D71' }}>{balance} <span className="monetary_unit">₽</span></span>
            </div>
          )}

          {(user.account && user.email_verified_at) && (
            <>
              <Button onClick={() => setOpenPayment(true)} color="green" size="small" label={text.deposit} />
              {openPayment ? <PaymentWindow hide={() => setOpenPayment(false)} accept={deposit} /> : null}
            </>
          )}

          {user.account && (
            <Link className="header_main_area_nav_menu_option header_main_right_menu_link" to="/cabinet" >
              <Cabinet className="header_main_area_nav_menu_icon" />
              {text.privateCabinet}
            </Link>
          )}

          <div className="header_main_area_nav_menu_option" onClick={logout} >
            <OutIcon className="header_main_area_nav_menu_icon" />
            {text.out}
          </div>

        </div>
      </div>
      <div className="header_main_area">
        <div className="header_main_logo">EDLY</div>
        <div className="header_main_area_nav_menu">
          <Link className="header_main_area_nav_menu_option" name="header_main_area_nav_menu_rooms" to={user.owner ? '/rooms' : '/webinars'}>
            <WebinarIcon className="header_main_area_nav_menu_icon" />
            {user.owner ? text.rooms : text.webinars}
          </Link>
          {user.account && (
            <Link className="header_main_area_nav_menu_option" name="header_main_area_nav_menu_cashbox" to="/cashbox">
              <CashBox className="header_main_area_nav_menu_icon" />
              {text.cashBox}
            </Link>
          )}
          {user.account && (
            <div className="header_main_area_nav_menu_balance">
              {text.balance}:&nbsp;<span style={{ color: balance > 0 ? '#00E096' : '#FF3D71' }}>{balance} <span className="monetary_unit">₽</span></span>
            </div>
          )}
          <div onClick={() => { setOpenMenu(true); document.getElementById('header_main_right_menu').focus() }} className="header_name">
            {user.first_name ? user.first_name[0] : '?'}
          </div>
          {!user.account || balance > 0 ? null : (
            <div className="header_main_popup_balance">
              {text.popupBalance}
            </div>
          )}
        </div>
      </div>
    </header>
  )
}

export default Header

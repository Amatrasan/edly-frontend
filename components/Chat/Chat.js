import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './UserChat.scss'
import '../CommonStyles/CommonStylesForm.scss'
import '../CommonStyles/CommonStylesChat.scss'
import PaymentWindow from '../PaymentWindow/PaymentPopUp'
import Footer from './ChatFooter'
import MessageList from './MessageList'
import Header from './ChatHeader'
import { postChatMessages } from '../../lib/app/chat/actions'
import { useSelector, useDispatch } from 'react-redux'
import useScreenInfo from '../../utils/useScreenInfo'
import { WebinarTypeTabsInChat } from '../../dict/webinar'

const Chat = (props) => {
  const user = useSelector(state => state.app.user)
  const chat = useSelector(state => state.app.chat?.[props.webinar.id] || [])
  const dispatch = useDispatch()
  const screenInfo = useScreenInfo()

  const [toWhom, setToWhom] = useState({ name: 'Всем', id: null })
  const [changeBanner, setChangeBanner] = useState('')
  const [openPayment, setOpenPayment] = useState(false)
  const [openSmile, setOpenSmile] = useState(false)

  // for moderate
  const [messageIdList, setMessageIdList] = useState([])
  const [showModerate, setShowModerate] = useState(false)
  // banners pop-up admin
  const [showBanners, setShowBanners] = useState(false)

  const checkMessage = (i, admin, idMessage) => {
    const message = document.getElementById(`chatMessageId${idMessage}`)
    const checkbox = document.getElementById(`chatMessageCheckboxId${idMessage}`)

    const textMessage = message.getElementsByClassName('admin_chat_message_text')
    const tempArray = [...messageIdList]
    if (messageIdList.indexOf(idMessage) !== -1) {
      tempArray.splice(messageIdList.indexOf(idMessage), 1)
      if (textMessage.length) {
        !admin ? textMessage[0].style.background = '#F7F9FC' : textMessage[0].style.background = '#FFF1C2'
        textMessage[0].style.color = '#222B45'
      }
      checkbox.style.opacity = '0'
    } else {
      if (textMessage.length) {
        textMessage[0].style.background = '#3366FF'
        textMessage[0].style.color = '#FFFFFF'
      }
      checkbox.style.opacity = '1'
      tempArray.push(idMessage)
    }
    setMessageIdList(tempArray)
  }

  const clearChatModeration = () => {
    setShowModerate(false)
    const temp = [...messageIdList]
    for (let i = 0; i < temp.length; i++) {
      const index = getIndex(temp[i])
      if (index) {
        const message = document.getElementById(`chatMessageId${temp[i]}`)
        const checkbox = document.getElementById(`chatMessageCheckboxId${temp[i]}`)

        const textMessage = message.getElementsByClassName('admin_chat_message_text')
        if (textMessage.length) {
          !index.admin ? textMessage[0].style.background = '#F7F9FC' : textMessage[0].style.background = '#FFF1C2'
          textMessage[0].style.color = '#222B45'
        }
        checkbox.style.opacity = '0'
      }
    }
    setMessageIdList([])
  }

  const getIndex = (id) => {
    for (let i = 0; i < chat.messages.length; i++) {
      if (chat.messages[i].id === id) {
        return { i: i, admin: chat.messages[i].sender_user_id === props.webinar.author_user_id }
      }
    }
  }

  const hideSmile = () => {
    const emoji = document.getElementsByClassName('emoji_main')
    if (emoji[0]) {
      emoji[0].style.display = 'none'
    }
    setOpenSmile(false)
  }

  const showSmile = () => {
    const emoji = document.getElementsByClassName('emoji_main')
    const btn = document.getElementsByClassName('chat_input_smile')
    if (!openSmile) {
      const res = getCoords(btn[0])
      emoji[0].style.display = 'block'
      emoji[0].style.top = `${res.top - 200}px`
      if (document.documentElement.clientWidth < 416) {
        emoji[0].style.width = '100%'
        emoji[0].style.left = '0'
      } else {
        emoji[0].style.left = `${res.left - 300 + 60}px`
      }
      setOpenSmile(true)
    } else {
      emoji[0].style.display = 'none'
      setOpenSmile(false)
    }
  }

  const getCoords = (elem) => {
    const box = elem.getBoundingClientRect()
    return {
      top: box.top + window.pageYOffset,
      left: box.left + window.pageXOffset,
      right: box.right + window.pageXOffset
    }
  }

  const header = document.getElementById('webinar_header')
  const style = header ? window.getComputedStyle(header) : {}

  const submitMessage = (textMessage, recipient, bannerId = null) => {
    const chatId = props.webinar.chat_id
    const webinarId = props.webinar.id
    if (bannerId) {
      dispatch(postChatMessages(webinarId, chatId, {
        banner_id: bannerId
      }))
    } else {
      dispatch(postChatMessages(webinarId, chatId, {
        message: textMessage,
        recipient_user_id: recipient.id
      }))
    }
  }

  const height = {
    height: screenInfo.width <= 415 ? `calc(${screenInfo.height}px - 200px - 44px)` : screenInfo.width <= 900 ? `calc(${screenInfo.height}px - 329px - 44px)` : `calc(100vh - ${style.height || 0} - ${style.marginBottom || 0})`
  }
  return (
    <>
      <PaymentWindow open={openPayment} setOpenPayment={setOpenPayment} bannerId={changeBanner}/>
      <div style={height} className="chat_main chat_main_height">
        <Header
          user={user}
          usersOnline={props.usersOnline}
          openHelp={props.openHelp}
          moderator={props.moderator}
          messageIdList={messageIdList}
          showModerate={showModerate}
          setShowModerate={setShowModerate}
          clearChatModeration={clearChatModeration}
          setShowBanners={setShowBanners}
          setOpenSmile={setOpenSmile}
          webinarId={props.webinar.id}
        />
        <MessageList
          {...props}
          user={user} checkMessage={checkMessage}
          setToWhom={setToWhom}
          setChangeBanner={setChangeBanner}
          setOpenPayment={setOpenPayment}
        />
        {(props.webinar.tabInChat === WebinarTypeTabsInChat.CHAT || screenInfo.width > 900) && <Footer
          {...props}
          submitMessage={submitMessage}
          block={Object.prototype.hasOwnProperty.call(props.webinar, 'chat_enabled') && props.webinar.chat_enabled === false}
          setShowBanners={setShowBanners}
          showBanners={showBanners}
          toWhom={toWhom}
          setToWhom={setToWhom}
          hideSmile={hideSmile}
          showSmile={showSmile}
        />}
      </div>
    </>
  )
}

Chat.propTypes = {
  block: PropTypes.bool.isRequired,
  webinar: PropTypes.shape({
    id: PropTypes.number.isRequired,
    author_user_id: PropTypes.number.isRequired,
    chat_enabled: PropTypes.bool.isRequired,
    adminable: PropTypes.bool.isRequired,
    moderatable: PropTypes.bool.isRequired,
    video_id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    tabInChat: PropTypes.string,
    amountOnline: PropTypes.number.isRequired,
    chat_id: PropTypes.number.isRequired,
    chat: PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  }).isRequired,
  openHelp: PropTypes.func.isRequired,
  moderator: PropTypes.bool,
  usersOnline: PropTypes.number.isRequired
}

export default Chat

import React, { useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Virtuoso } from 'react-virtuoso'
import ProductInChat from '../Products/ProductsInChat'
import './AdminChat.scss'
import '../CommonStyles/CommonStylesForm.scss'
import '../CommonStyles/CommonStylesChat.scss'
import { useDispatch, useSelector } from 'react-redux'
import { getChatMessages, unblockChatForEveryone } from '../../lib/app/chat/actions'
import Message from './Message'
import { WebinarTypeTabsInChat } from '../../dict/webinar'
import useScreenInfo from '../../utils/useScreenInfo'
import { Unlock } from '../UI/Icons/Icons'
import Button from '../UI/Button'

const MessageList = (props) => {
  const screenInfo = useScreenInfo()
  const dispatch = useDispatch()
  const messages = useSelector(state => state.app.chat?.[props.webinar.id]?.messages)
  const pagination = useSelector(state => state.app.chat?.[props.webinar.id]?.pagination)
  const visibleRange = useRef([0, 0])
  const [isScrolling, setIsScrolling] = useState(false)

  const virtuoso = useRef(null)
  const loading = useRef(false)

  useEffect(() => {
    if (virtuoso.current && messages && (messages.length - 1 - visibleRange.current[1] < 4)) {
      setTimeout(() => {
        virtuoso.current.scrollToIndex({ index: messages.length - 1, align: 'end' })
      }, 80)
    }

  }, [messages])

  const loadPrevious = () => {
    if (loading.current || !props.webinar?.chat_id) {
      return
    }

    loading.current = true

    if (pagination.current_page > 1) {
      dispatch(
        getChatMessages(props.webinar.id, props.webinar.chat_id, pagination.current_page - 1))
        .then(() => {
          loading.current = false
          if (virtuoso.current) {
            virtuoso.current.adjustForPrependedItems(pagination.per_page)
          }
        })
    }
  }

  const unblockChat = () => {
    dispatch(unblockChatForEveryone(props.webinar.chat_id))
  }

  const MessageListItem = index => {
    return <Message
      isScrolling={isScrolling}
      message={messages[index]}
      index={index}
      setToWhom={props.setToWhom}
      checkMessage={props.checkMessage}
      webinarId={props.webinar.id}
    />
  }

  return (
    <div>
      <div className="chat_banners_list_main"
        style={{ display: props.webinar.tabInChat === WebinarTypeTabsInChat.PRODUCTS && screenInfo.width < 900 ? 'block' : 'none' }}>
        <ProductInChat webinarId={props.webinar.id}/>
      </div>
      <div className="chat_list" id="chat_window" style={{ display: props.webinar.tabInChat === WebinarTypeTabsInChat.CHAT || screenInfo.width > 900 ? 'block' : 'none' }}>
        {!props.webinar.chat_enabled &&
        <div className="chat_list_block">
          <Unlock className="chat_list_block_icon"/>
          <span>Чат заблокирован</span>
          {(props.webinar.adminable || props.webinar.moderatable) &&
          <Button
            size="small"
            className="button_webinar"
            color="green"
            onClick={unblockChat}
          >
            <Unlock/>
            Разблокировать
          </Button>}
        </div>}
        {!!messages?.length && (
          <Virtuoso
            ref={virtuoso}
            style={{ height: '100%' }}
            item={MessageListItem}
            totalCount={messages.length}
            initialTopMostItemIndex={messages && messages.length > 1 ? messages.length - 1 : 0}
            scrollingStateChange={scrolling => {
              setIsScrolling(scrolling)
            }}
            rangeChanged={({ startIndex, endIndex }) => {
              // Scrolling up
              if (startIndex < visibleRange.current[0]) {
                // Eager loading
                if (startIndex && startIndex <= 30) {
                  loadPrevious()
                }
              }
              visibleRange.current = [startIndex, endIndex]
            }}
          />
        )}
      </div>
    </div>
  )
}

MessageList.propTypes = {
  setToWhom: PropTypes.func,
  setChangeBanner: PropTypes.func,
  setOpenPayment: PropTypes.func,
  checkMessage: PropTypes.func.isRequired,
  user: PropTypes.object,
  webinar: PropTypes.shape({
    id: PropTypes.number,
    tabInChat: PropTypes.string,
    chat_id: PropTypes.number.isRequired,
    chat_enabled: PropTypes.bool,
    adminable: PropTypes.bool.isRequired,
    moderatable: PropTypes.bool
  })
}

export default MessageList

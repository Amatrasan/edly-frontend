import React from 'react'
import './Message.scss'
import Banner from '../../Banner'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

const Message = (props) => {
  const { message, index } = props
  const isAdmin = useSelector(state => state.app.webinar[props.webinarId]?.adminable)
  const user = useSelector(state => state.app.user)
  const isModerator = useSelector(state => state.app.webinar[props.webinarId]?.moderatable)
  const authorUserId = useSelector(state => state.app.webinar[props.webinarId]?.author_user_id)

  const MessageListItemForAdmin = (data, i) => {
    const moderator = authorUserId === data.sender_user_id
    if (data.banner) {
      return (
        <React.Fragment key={i}>
          <div id={`chatMessageId${data.id}`} style={{ textAlign: 'left' }}
            className="admin_chat_message"
            onClick={() => props.checkMessage(i, moderator, data.id)}>
            <strong>{data.author} {moderator
              ? '[модератор]'
              : ''}</strong>&nbsp;&nbsp;{new Date(
              data.date).toLocaleTimeString().substr(0, 5)}
            <div className="checkbox_flex">
              <Banner
                banner={data.banner}
                context="chat"
                isScrolling={props.isScrolling}
              />
              <div className="checkbox_flex">
                <svg id={`chatMessageCheckboxId${data.id}`}
                  className="checkbox_message" width="24" height="24"
                  viewBox="0 0 24 24" fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <rect x="3" y="3" width="18" height="18" rx="4"
                    fill="url(#paint0_linear)"/>
                  <path fillRule="evenodd" clipRule="evenodd"
                    d="M14.0385 9.42926C14.3926 8.96204 15.0718 8.86049 15.5555 9.20244C16.0393 9.54439 16.1444 10.2004 15.7904 10.6676L12.4312 15.1005C12.0688 15.5787 11.3697 15.6323 10.9386 15.215L8.31701 12.677C7.89362 12.2671 7.89443 11.6033 8.31884 11.1944C8.74324 10.7855 9.43052 10.7862 9.85391 11.1961L11.4954 12.7853L14.0385 9.42926Z"
                    fill="white"/>
                  <defs>
                    <linearGradient id="paint0_linear" x1="12" y1="21" x2="12"
                      y2="3"
                      gradientUnits="userSpaceOnUse">
                      <stop stopColor="#3366FF"/>
                      <stop offset="1" stopColor="#598BFF"/>
                    </linearGradient>
                  </defs>
                </svg>
              </div>
            </div>
          </div>

        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment key={i}>
          <div id={`chatMessageId${data.id}`} className="admin_chat_message">
            <div style={{ display: 'flex', textAlign: 'left' }} onClick={() => {
              if (user.id !== data.sender_user_id) {
                props.setToWhom({ name: data.author, id: data.sender_user_id })
              }
            }}>
              <strong>{data.author} {moderator
                ? '[модератор]'
                : ''} </strong>&nbsp;&nbsp;{new Date(
                data.date).toLocaleTimeString().substr(0, 5)}
              {data.recipient_user_id ? (
                <div className="admin_chat_message_flex_icon">
                  &nbsp;
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M12 12C12.5523 12 13 11.5523 13 11C13 10.4477 12.5523 10 12 10C11.4477 10 11 10.4477 11 11C11 11.5523 11.4477 12 12 12Z"
                      fill="#8F9BB3"/>
                    <path
                      d="M16 12C16.5523 12 17 11.5523 17 11C17 10.4477 16.5523 10 16 10C15.4477 10 15 10.4477 15 11C15 11.5523 15.4477 12 16 12Z"
                      fill="#8F9BB3"/>
                    <path
                      d="M8 12C8.55228 12 9 11.5523 9 11C9 10.4477 8.55228 10 8 10C7.44772 10 7 10.4477 7 11C7 11.5523 7.44772 12 8 12Z"
                      fill="#8F9BB3"/>
                    <path
                      d="M19 3H5C4.20435 3 3.44129 3.31607 2.87868 3.87868C2.31607 4.44129 2 5.20435 2 6V21C2.00031 21.1772 2.04769 21.3511 2.1373 21.504C2.22691 21.6569 2.35553 21.7832 2.51 21.87C2.65946 21.9547 2.82821 21.9995 3 22C3.17948 21.9999 3.35564 21.9516 3.51 21.86L8 19.14C8.16597 19.0412 8.35699 18.9926 8.55 19H19C19.7956 19 20.5587 18.6839 21.1213 18.1213C21.6839 17.5587 22 16.7956 22 16V6C22 5.20435 21.6839 4.44129 21.1213 3.87868C20.5587 3.31607 19.7956 3 19 3ZM20 16C20 16.2652 19.8946 16.5196 19.7071 16.7071C19.5196 16.8946 19.2652 17 19 17H8.55C8.00382 16.9996 7.46789 17.1482 7 17.43L4 19.23V6C4 5.73478 4.10536 5.48043 4.29289 5.29289C4.48043 5.10536 4.73478 5 5 5H19C19.2652 5 19.5196 5.10536 19.7071 5.29289C19.8946 5.48043 20 5.73478 20 6V16Z"
                      fill="#8F9BB3"/>
                  </svg>
                  <div
                    className="admin_chat_message_recipient_name">{data.recipient_name}</div>
                  &nbsp;
                </div>
              ) : ''}
            </div>
            <div className="checkbox_flex">
              <div onClick={() => props.checkMessage(i, moderator, data.id)}
                className={`admin_chat_message_text ${moderator
                  ? 'admin_message_admin'
                  : ''}`}>
                {data.text}
              </div>
              <div className="checkbox_flex">
                <svg id={`chatMessageCheckboxId${data.id}`}
                  className="checkbox_message" width="24" height="24"
                  viewBox="0 0 24 24" fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <rect x="3" y="3" width="18" height="18" rx="4"
                    fill="url(#paint0_linear)"/>
                  <path fillRule="evenodd" clipRule="evenodd"
                    d="M14.0385 9.42926C14.3926 8.96204 15.0718 8.86049 15.5555 9.20244C16.0393 9.54439 16.1444 10.2004 15.7904 10.6676L12.4312 15.1005C12.0688 15.5787 11.3697 15.6323 10.9386 15.215L8.31701 12.677C7.89362 12.2671 7.89443 11.6033 8.31884 11.1944C8.74324 10.7855 9.43052 10.7862 9.85391 11.1961L11.4954 12.7853L14.0385 9.42926Z"
                    fill="white"/>
                  <defs>
                    <linearGradient id="paint0_linear" x1="12" y1="21" x2="12"
                      y2="3" gradientUnits="userSpaceOnUse">
                      <stop stopColor="#3366FF"/>
                      <stop offset="1" stopColor="#598BFF"/>
                    </linearGradient>
                  </defs>
                </svg>
              </div>
            </div>
          </div>

        </React.Fragment>
      )
    }
  }
  const MessageListItemForUser = (data, i) => {
    const moderator = authorUserId === data.sender_user_id

    if (data.banner) {
      return (
        <React.Fragment key={i}>
          <div id={`chatMessageId${data.id}`} className="chat_message"
            style={{ textAlign: 'left' }}>
            <strong>{data.author} {moderator
              ? '[модератор]'
              : ''}</strong>&nbsp;&nbsp;{new Date(
              data.date).toLocaleTimeString().substr(0, 5)}
            <Banner
              isScrolling={props.isScrolling}
              banner={data.banner}
              context="chat"
            />
          </div>
        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment key={i}>
          <div id={`chatMessageId${data.id}`} className="chat_message">
            <div onClick={() => {
              if (user.id !== data.sender_user_id) {
                props.setToWhom({ name: data.author, id: data.sender_user_id })
              }
            }}>
              <strong>{data.author} {moderator
                ? '[модератор]'
                : ''}{data.recipient_user_id ? (
                <>
                  &nbsp;
                  <svg className="chat_message_right_arrow" width="21"
                    height="21"
                    viewBox="0 0 21 21" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M5.875 13H16.2525L13.0763 16.815C13.0027 16.9035 12.9473 17.0056 12.9132 17.1155C12.8792 17.2253 12.867 17.3409 12.8776 17.4554C12.8989 17.6868 13.0113 17.9002 13.19 18.0488C13.3687 18.1973 13.5991 18.2687 13.8304 18.2474C14.0618 18.2261 14.2752 18.1137 14.4237 17.935L18.7987 12.685C18.8282 12.6432 18.8545 12.5994 18.8775 12.5538C18.8775 12.51 18.9213 12.4838 18.9388 12.44C18.9784 12.3397 18.9992 12.2329 19 12.125C18.9992 12.0171 18.9784 11.9103 18.9388 11.81C18.9388 11.7663 18.895 11.74 18.8775 11.6963C18.8545 11.6506 18.8282 11.6068 18.7987 11.565L14.4237 6.315C14.3415 6.21623 14.2385 6.1368 14.122 6.08235C14.0056 6.02791 13.8785 5.99979 13.75 6C13.5456 5.9996 13.3474 6.07081 13.19 6.20125C13.1014 6.27471 13.0282 6.36492 12.9745 6.46672C12.9208 6.56853 12.8877 6.67992 12.8772 6.79453C12.8666 6.90913 12.8787 7.02469 12.9129 7.1346C12.9471 7.2445 13.0026 7.34658 13.0763 7.435L16.2525 11.25H5.875C5.64294 11.25 5.42038 11.3422 5.25628 11.5063C5.09219 11.6704 5 11.8929 5 12.125C5 12.3571 5.09219 12.5796 5.25628 12.7437C5.42038 12.9078 5.64294 13 5.875 13Z"
                      fill="#222B45"/>
                  </svg>
                  &nbsp;
                  {user.id === data.recipient_user_id
                    ? 'мне'
                    : data.recipient_name}
                </>
              ) : ''} </strong>&nbsp;&nbsp;{new Date(
                data.date).toLocaleTimeString().substr(0, 5)}
            </div>
            <div
              className={`chat_message_text ${moderator
                ? 'message_admin'
                : ''} ${data.recipient_user_id ? 'message_personal' : ''}`}>
              {data.text}
            </div>
          </div>
        </React.Fragment>
      )
    }
  }
  return (
    (isAdmin || isModerator)
      ? MessageListItemForAdmin(message, index)
      : MessageListItemForUser(message, index)
  )
}

Message.propTypes = {
  checkMessage: PropTypes.func.isRequired,
  setToWhom: PropTypes.func.isRequired,
  index: PropTypes.number,
  isScrolling: PropTypes.bool.isRequired,
  message: PropTypes.shape({
    id: PropTypes.number.isRequired,
    sender_user_id: PropTypes.number.isRequired,
    recipient_user_id: PropTypes.number,
    text: PropTypes.string,
    banner: PropTypes.object,
    author: PropTypes.string,
    date: PropTypes.string.isRequired
  }).isRequired
}

export default Message
